#!/bin/bash

sudo timedatectl set-ntp true
sudo hwclock --systohc

sudo reflector -c Switzerland -a 6 --sort rate --save /etc/pacman.d/mirrorlist

paru -S --noconfirm auto-cpufreq
sudo systemctl enable --now auto-cpufreq

sudo pacman -S --noconfirm gdm gnome gnome-extra firefox gnome-tweaks papirus-icon-theme vlc bdf-unifont \
gnu-free-fonts ttf-roboto ttf-hack ttf-fira-code ttf-jetbrains-mono adobe-source-code-pro-fonts cantarell-fonts \
ttf-opensans  noto-fonts-emoji ttf-font-awesome

sudo flatpak install -y spotify

sudo systemctl enable gdm
/bin/echo -e "\e[1;32mREBOOTING IN 5..4..3..2..1..\e[0m"
sleep 5
